-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: devcatalog.create-it.solutions    Database: devcatalog
-- ------------------------------------------------------
-- Server version	5.5.49-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  `activated` tinyint(1) NOT NULL,
  `sanitizeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleSeo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descriptionSeo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hasWishilst` tinyint(1) NOT NULL,
  `$hasAllList` tinyint(1) NOT NULL,
  `className` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'Kiki','monchhichi.png','2017-03-05',1,'kiki','Kiki - Cat(alog)',NULL,'Kiki est une peluche de taille variable représentant un petit singe avec une tête ronde et dure, une tétine à la main, et qui connut un succès international dans les années 1980.',1,1,'Monchhichi'),(2,'Nanoblock','lego.png','2017-03-05',1,'nanoblock','Nanoblock - Cat(alog)',NULL,'Lancés en 2008 par le fabricant japonais Kawada, les Nanoblocks sont des versions miniatures des Lego. Une brique Nanoblock de base fait environ la moitié de la largeur d’une brique Lego équivalente, et un tiers de sa hauteur.',1,1,'Nanoblock'),(4,'Minis','teeturtle-coupon.png','2017-05-05',0,'minis',NULL,NULL,'Les Minis sont de joyeuses peluches vendues par TeeTurtle. A ce jour, il existe les KittenCorn, les PuppiCorn, Angry Bunny et les Narwhals.',0,0,'Minis'),(5,'Funko Pop!','funko-pop-logo.png','2017-05-05',0,'funko-pop',NULL,NULL,'Les figurines Funko Pop sont des figurines de collection en vinyl créées par la marque américaine Funko. Ces figurines se démarquent bien sûr par leur fameuse très grosse tête et un design simplifié, bien qu\'elles soient de plus en plus riches en détails.',0,0,'FunkoPop');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_binarydata`
--

DROP TABLE IF EXISTS `phpcr_binarydata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_binarydata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL,
  `property_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `workspace_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idx` int(11) NOT NULL DEFAULT '0',
  `data` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_37E65615460D9FD7413BC13C1AC10DC4E7087E10` (`node_id`,`property_name`,`workspace_name`,`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_binarydata`
--

LOCK TABLES `phpcr_binarydata` WRITE;
/*!40000 ALTER TABLE `phpcr_binarydata` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_binarydata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_internal_index_types`
--

DROP TABLE IF EXISTS `phpcr_internal_index_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_internal_index_types` (
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `node_id` int(11) NOT NULL,
  PRIMARY KEY (`type`,`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_internal_index_types`
--

LOCK TABLES `phpcr_internal_index_types` WRITE;
/*!40000 ALTER TABLE `phpcr_internal_index_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_internal_index_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_namespaces`
--

DROP TABLE IF EXISTS `phpcr_namespaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_namespaces` (
  `prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_namespaces`
--

LOCK TABLES `phpcr_namespaces` WRITE;
/*!40000 ALTER TABLE `phpcr_namespaces` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_namespaces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_nodes`
--

DROP TABLE IF EXISTS `phpcr_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namespace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `workspace_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `props` longtext COLLATE utf8_unicode_ci NOT NULL,
  `numerical_props` longtext COLLATE utf8_unicode_ci,
  `depth` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A4624AD7B548B0F1AC10DC4` (`path`,`workspace_name`),
  UNIQUE KEY `UNIQ_A4624AD7772E836A1AC10DC4` (`identifier`,`workspace_name`),
  KEY `IDX_A4624AD73D8E604F` (`parent`),
  KEY `IDX_A4624AD78CDE5729` (`type`),
  KEY `IDX_A4624AD7623C14D533E16B56` (`local_name`,`namespace`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_nodes`
--

LOCK TABLES `phpcr_nodes` WRITE;
/*!40000 ALTER TABLE `phpcr_nodes` DISABLE KEYS */;
INSERT INTO `phpcr_nodes` VALUES (1,'/','','','','default','3041d3b4-24c4-413f-8be7-e52783b5a936','nt:unstructured','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<sv:node xmlns:mix=\"http://www.jcp.org/jcr/mix/1.0\" xmlns:nt=\"http://www.jcp.org/jcr/nt/1.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:jcr=\"http://www.jcp.org/jcr/1.0\" xmlns:sv=\"http://www.jcp.org/jcr/sv/1.0\" xmlns:rep=\"internal\" />',NULL,0,NULL);
/*!40000 ALTER TABLE `phpcr_nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_nodes_references`
--

DROP TABLE IF EXISTS `phpcr_nodes_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_nodes_references` (
  `source_id` int(11) NOT NULL,
  `source_property_name` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(11) NOT NULL,
  PRIMARY KEY (`source_id`,`source_property_name`,`target_id`),
  KEY `IDX_F3BF7E1158E0B66` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_nodes_references`
--

LOCK TABLES `phpcr_nodes_references` WRITE;
/*!40000 ALTER TABLE `phpcr_nodes_references` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_nodes_references` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_nodes_weakreferences`
--

DROP TABLE IF EXISTS `phpcr_nodes_weakreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_nodes_weakreferences` (
  `source_id` int(11) NOT NULL,
  `source_property_name` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(11) NOT NULL,
  PRIMARY KEY (`source_id`,`source_property_name`,`target_id`),
  KEY `IDX_F0E4F6FA158E0B66` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_nodes_weakreferences`
--

LOCK TABLES `phpcr_nodes_weakreferences` WRITE;
/*!40000 ALTER TABLE `phpcr_nodes_weakreferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_nodes_weakreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_type_childs`
--

DROP TABLE IF EXISTS `phpcr_type_childs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_type_childs` (
  `node_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `protected` tinyint(1) NOT NULL,
  `auto_created` tinyint(1) NOT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `on_parent_version` int(11) NOT NULL,
  `primary_types` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_type_childs`
--

LOCK TABLES `phpcr_type_childs` WRITE;
/*!40000 ALTER TABLE `phpcr_type_childs` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_type_childs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_type_nodes`
--

DROP TABLE IF EXISTS `phpcr_type_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_type_nodes` (
  `node_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supertypes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_abstract` tinyint(1) NOT NULL,
  `is_mixin` tinyint(1) NOT NULL,
  `queryable` tinyint(1) NOT NULL,
  `orderable_child_nodes` tinyint(1) NOT NULL,
  `primary_item` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`node_type_id`),
  UNIQUE KEY `UNIQ_34B0A8095E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_type_nodes`
--

LOCK TABLES `phpcr_type_nodes` WRITE;
/*!40000 ALTER TABLE `phpcr_type_nodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_type_nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_type_props`
--

DROP TABLE IF EXISTS `phpcr_type_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_type_props` (
  `node_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `protected` tinyint(1) NOT NULL,
  `auto_created` tinyint(1) NOT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `on_parent_version` int(11) NOT NULL,
  `multiple` tinyint(1) NOT NULL,
  `fulltext_searchable` tinyint(1) NOT NULL,
  `query_orderable` tinyint(1) NOT NULL,
  `required_type` int(11) NOT NULL,
  `query_operators` int(11) NOT NULL,
  `default_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`node_type_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_type_props`
--

LOCK TABLES `phpcr_type_props` WRITE;
/*!40000 ALTER TABLE `phpcr_type_props` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpcr_type_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpcr_workspaces`
--

DROP TABLE IF EXISTS `phpcr_workspaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpcr_workspaces` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpcr_workspaces`
--

LOCK TABLES `phpcr_workspaces` WRITE;
/*!40000 ALTER TABLE `phpcr_workspaces` DISABLE KEYS */;
INSERT INTO `phpcr_workspaces` VALUES ('default');
/*!40000 ALTER TABLE `phpcr_workspaces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `purchaseDate` date DEFAULT NULL,
  `creationDate` date NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  `wishlist` tinyint(1) NOT NULL,
  `productType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purchasePrice` decimal(10,2) DEFAULT NULL,
  `sellingPrice` decimal(10,2) DEFAULT NULL,
  `nbPieces` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Panda','2000-12-25','2017-02-19','P1150045-min.JPG','2017-02-19',0,'monchhichi',NULL,NULL,NULL),(2,'Ecureuil','2016-12-11','2017-02-19','ecureuil.jpg','2017-02-19',0,'nanoblock',NULL,NULL,130),(3,'Golden Retriever','2016-12-11','2017-02-20','golden-retriver.jpg','2017-02-20',0,'nanoblock',NULL,NULL,140),(4,'Lion','2016-12-11','2017-02-20','lion.jpg','2017-02-20',0,'nanoblock',NULL,NULL,140),(5,'Loup','2016-12-11','2017-02-20','mini-series-nanoblock-gray-wolf.jpg','2017-02-20',0,'nanoblock',NULL,NULL,130),(6,'Panda','2016-12-11','2017-02-20','mini-series-nanoblock-giant-panda.jpg','2017-02-20',0,'nanoblock',NULL,NULL,170),(7,'Panda roux','2016-12-11','2017-02-20','panda-roux.jpg','2017-02-20',0,'nanoblock',NULL,NULL,130),(8,'Perruche ondulée verte','2016-12-11','2017-02-20','1__88394.1449044395.1280.1280.jpg','2017-02-20',0,'nanoblock',NULL,NULL,80),(9,'Tigre','2016-12-11','2017-02-20','nanoblock_bengal_tiger.jpg','2017-02-20',0,'nanoblock',NULL,NULL,160),(10,'Kiki 80cm',NULL,'2017-02-20','P1150112-min.JPG','2017-02-20',0,'monchhichi',180.00,NULL,NULL),(11,'Bourriquet','2014-08-07','2017-03-03','P1150023-min.JPG','2017-03-03',0,'monchhichi',25.00,NULL,NULL),(12,'Ours-Abeille','2014-08-16','2017-03-03','P1150024-min.JPG','2017-03-03',0,'monchhichi',25.00,NULL,NULL),(13,'Punk Boy','2014-11-23','2017-03-05','P1150180-min.JPG','2017-03-05',0,'monchhichi',23.10,NULL,NULL),(14,'Rody bleu (couché)','2007-08-23','2017-03-05','P1150135-min.JPG','2017-03-05',0,'monchhichi',NULL,NULL,NULL),(15,'Rody bleu (debout)','2007-08-23','2017-03-05','P1150131-min.JPG','2017-03-05',0,'monchhichi',NULL,NULL,NULL),(16,'Rody rouge (couché)','2007-08-23','2017-03-05','P1150133-min.JPG','2017-03-05',0,'monchhichi',NULL,NULL,NULL),(17,'Rody rouge (debout)','2007-08-23','2017-03-05','P1150175-min.JPG','2017-03-05',0,'monchhichi',NULL,NULL,NULL),(18,'Rody jaune (couché)','2007-08-23','2017-03-05','P1150134-min.JPG','2017-03-05',0,'monchhichi',NULL,NULL,NULL),(19,'Rody jaune (debout)','2007-08-23','2017-03-05','P1150130-min.JPG','2017-03-05',0,'monchhichi',NULL,NULL,NULL),(20,'Kiki Nanoblock','2016-07-17','2017-03-05','P1150184-min.JPG','2017-03-05',0,'monchhichi',41.35,NULL,NULL),(21,'Kiki','2016-07-17','2017-03-05','P1150184-min.JPG','2017-03-05',0,'nanoblock',NULL,NULL,190),(22,'Kiki Fleur Girl avec bouquet',NULL,'2017-03-15','258340-800x800.jpg','2017-03-15',1,'monchhichi',NULL,25.42,NULL),(23,'Kiki Ours Polaire',NULL,'2017-03-15','259458a-800x800.JPG','2017-03-15',1,'monchhichi',NULL,28.24,NULL),(24,'Christmas X\'Mas Green','2015-11-29','2017-04-10','P1150078-min.JPG','2017-04-10',0,'monchhichi',17.99,NULL,NULL),(25,'Chat noir',NULL,'2017-04-10','P1150080-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(26,'Poussin',NULL,'2017-04-10','P1150081-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(27,'Lapin rose',NULL,'2017-04-10','P1150082-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(28,'Chat kawaii',NULL,'2017-04-10','P1150083-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(29,'Cochon',NULL,'2017-04-10','P1150085-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(30,'Lapin kawaii',NULL,'2017-04-10','P1150086-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(31,'Chat gris',NULL,'2017-04-10','P1150087-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(32,'Chat marron',NULL,'2017-04-10','P1150088-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(33,'Sagittaire',NULL,'2017-04-10','P1150069-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(34,'Girl bavette rose',NULL,'2017-04-10','P1150072-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(35,'Kiki porte-clé','2005-01-01','2017-04-10','P1150073-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(36,'Kiki porte-clé',NULL,'2017-04-10','P1150077-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(37,'Girl blanche',NULL,'2017-04-10','P1150084-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(38,'Kiki porte-clé','2005-01-01','2017-04-10','P1150090-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(39,'Kiki porte-clé','2005-01-01','2017-04-10','P1150094-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(40,'Maki poulpe',NULL,'2017-04-10','P1150132-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(41,'Maki oursin',NULL,'2017-04-10','P1150140-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(42,'Maki thon',NULL,'2017-04-10','P1150141-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(43,'Tirelire jaune',NULL,'2017-04-10','P1150062-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(44,'Tirelire rouge','2007-08-23','2017-04-10','P1150183-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(45,'Kiki Girl 80cm','2007-12-25','2017-04-10','P1150013-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(46,'Kiki Boy 80cm','2007-12-25','2017-04-10','P1150014-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(47,'Patissier Mont-Blanc','2007-08-23','2017-04-10','P1150121-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(48,'Les Mariés','2007-06-11','2017-04-10','P1150205-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(49,'Patissier Café au lait','2007-08-23','2017-04-10','P1150123-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(50,'Patissière Tarte aux fraises','2007-08-23','2017-04-10','P1150118-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(51,'Patissière Charlotte aux fraises','2007-08-23','2017-04-10','P1150119-min.JPG','2017-04-10',0,'monchhichi',NULL,NULL,NULL),(52,'Maman avec poussette','2016-12-09','2017-04-10','P1150034-min.JPG','2017-04-10',0,'monchhichi',20.00,NULL,NULL),(53,'Ours','2015-11-22','2017-04-10','P1150030-min.JPG','2017-04-10',0,'monchhichi',19.79,NULL,NULL),(54,'Denim Girl (40th Ann.)','2014-11-23','2017-04-12','P1150060-min.JPG','2017-04-12',0,'monchhichi',20.79,NULL,NULL),(55,'Platinum Boy (40th Ann.)','2014-11-23','2017-04-12','P1150074-min.JPG','2017-04-12',0,'monchhichi',20.76,NULL,NULL),(56,'Infirmière','2015-11-22','2017-04-12','P1150047-min.JPG','2017-04-12',0,'monchhichi',21.59,NULL,NULL),(57,'SnowBoarder Girl','2014-11-23','2017-04-12','P1150152-min.JPG','2017-04-12',0,'monchhichi',31.10,NULL,NULL),(58,'Skieur','2014-11-23','2017-04-12','P1150154-min.JPG','2017-04-12',0,'monchhichi',31.10,NULL,NULL),(59,'Chapeau & Canne','2015-11-22','2017-04-12','P1150029-min.JPG','2017-04-12',0,'monchhichi',21.59,NULL,NULL),(60,'Teddy Bear','2015-11-22','2017-04-12','P1150026-min.JPG','2017-04-12',0,'monchhichi',23.39,NULL,NULL),(61,'Grizzly','2017-01-19','2017-04-13','P1290017-min.JPG','2017-04-13',0,'monchhichi',19.41,NULL,NULL),(62,'Cochon','2017-01-23','2017-04-13','P1290015-min.JPG','2017-04-13',0,'monchhichi',22.14,NULL,NULL),(63,'Papillon','2007-01-07','2017-04-28','P1150058-min.JPG','2017-04-28',0,'monchhichi',NULL,NULL,NULL),(64,'Père Noël','2006-12-25','2017-04-28','P1150101-min.JPG','2017-04-28',0,'monchhichi',NULL,NULL,NULL),(65,'Fille - Festival Japonais','2017-01-16','2017-04-28','P1290024-min.JPG','2017-04-28',0,'monchhichi',6.80,NULL,NULL),(66,'Garçon - Festival Japonais','2017-01-16','2017-04-28','P1290023-min.JPG','2017-04-28',0,'monchhichi',6.80,NULL,NULL),(67,'Ours brun','2017-01-17','2017-04-28','P1290018-min.JPG','2017-04-28',0,'monchhichi',31.91,NULL,NULL),(68,'Renard','2017-01-17','2017-04-28','P1290016-min.JPG','2017-04-28',0,'monchhichi',23.20,NULL,NULL),(69,'Sarouel','2017-01-17','2017-04-28','P1290022-min.JPG','2017-04-28',0,'monchhichi',26.10,NULL,NULL),(70,'Servante','2017-01-17','2017-04-28','P1290021-min.JPG','2017-04-28',0,'monchhichi',26.10,NULL,NULL),(71,'Servant','2017-01-17','2017-04-28','P1290020-min.JPG','2017-04-28',0,'monchhichi',26.10,NULL,NULL),(72,'Chien (Umekichi)','2017-01-16','2017-04-28','P1290014-min.JPG','2017-04-28',0,'monchhichi',43.91,NULL,NULL),(73,'Vache (Année de la)','2017-01-16','2017-04-28','P1290012-min.JPG','2017-04-28',0,'monchhichi',23.41,NULL,NULL),(74,'Dracula','2014-11-24','2017-04-28','P1150043-min.JPG','2017-04-28',0,'monchhichi',32.54,NULL,NULL),(75,'Mods boy','2014-11-24','2017-04-28','P1150042-min.JPG','2017-04-28',0,'monchhichi',26.58,NULL,NULL),(76,'Ecolier','2014-11-24','2017-04-28','P1150035-min.JPG','2017-04-28',0,'monchhichi',32.54,NULL,NULL),(77,'Pompier','2007-08-10','2017-04-28','P1150096-min.JPG','2017-04-28',0,'monchhichi',29.90,NULL,NULL),(78,'Docteur','2007-08-10','2017-04-28','P1150107-min.JPG','2017-04-28',0,'monchhichi',29.90,NULL,NULL),(79,'Gendarme','2007-08-10','2017-04-28','P1150095-min.JPG','2017-04-28',0,'monchhichi',29.90,NULL,NULL),(80,'Infirmière','2007-08-10','2017-04-28','P1150106-min.JPG','2017-04-28',0,'monchhichi',29.90,NULL,NULL),(81,'Gendarmette','2007-08-10','2017-04-28','P1150108-min.JPG','2017-04-28',0,'monchhichi',29.90,NULL,NULL),(82,'Golfeur','2007-08-10','2017-04-28','P1150104-min.JPG','2017-04-28',0,'monchhichi',29.90,NULL,NULL),(83,'Mont St Michel',NULL,'2017-05-01','71UkaErXWuL._SX522_.jpg','2017-05-01',1,'nanoblock',NULL,NULL,340),(84,'Squelette tigre NONAGON',NULL,'2017-05-01','squelette-tigre-nonagon.jpg','2017-05-01',1,'nanoblock',NULL,NULL,720),(85,'Château de Neuschwanstein',NULL,'2017-05-01','schloss-neuschwanstein-deluxe-edition.jpg','2017-05-01',1,'nanoblock',NULL,NULL,5800),(86,'Voilier',NULL,'2017-05-01','voilier.jpg','2017-05-01',1,'nanoblock',NULL,NULL,1800),(87,'Hamsters',NULL,'2017-05-01','hamster.jpg','2017-05-01',1,'nanoblock',NULL,NULL,170),(88,'Poissons-Ballons Vert',NULL,'2017-05-01','green-spotted-puffer.jpg','2017-05-01',1,'nanoblock',NULL,NULL,150),(90,'Bière',NULL,'2017-05-01','ml-033_a__22825.1297951430.1280.1280.jpg','2017-05-01',1,'nanoblock',NULL,NULL,130),(91,'I <3 Dogs',NULL,'2017-05-01','ml-032_a__75426.1297951430.1280.1280.jpg','2017-05-01',1,'nanoblock',NULL,NULL,0),(92,'Perroquet',NULL,'2017-05-01','2__55569.1459755139.1280.1280.jpg','2017-05-01',1,'nanoblock',NULL,NULL,110),(93,'Koala',NULL,'2017-05-01','nanoblock-koala.jpg','2017-05-01',1,'nanoblock',NULL,NULL,150),(94,'Crocodile du Nil',NULL,'2017-05-01','nile-crocodile.jpg','2017-05-01',1,'nanoblock',NULL,NULL,100),(95,'Méduses',NULL,'2017-05-01','moon-jellyfish.jpg','2017-05-01',1,'nanoblock',NULL,NULL,200),(96,'Maman singe et son bébé',NULL,'2017-05-01','maman-singe-et-son-bebe.jpg','2017-05-01',1,'nanoblock',NULL,NULL,180),(97,'Poisson-clown et poisson palette',NULL,'2017-05-01','le-poisson-clown-et-le-chirurgien-bleu.jpg','2017-05-01',1,'nanoblock',NULL,NULL,180),(98,'Loutre cendrée',NULL,'2017-05-01','loutre-cendree.jpg','2017-05-01',1,'nanoblock',NULL,NULL,120),(99,'Phoque et son bébé',NULL,'2017-05-01','phoca-largha-et-son-bebe.jpg','2017-05-01',1,'nanoblock',NULL,NULL,130),(100,'Dauphin à flancs blancs du Pacifique',NULL,'2017-05-01','dauphin-a-flancs-blancs-du-pacifique.jpg','2017-05-01',1,'nanoblock',NULL,NULL,110),(101,'Coq',NULL,'2017-05-01','coq.jpg','2017-05-01',1,'nanoblock',NULL,NULL,100),(102,'Mon premier','1995-01-01','2017-05-20','P1150158-min.JPG','2017-05-20',0,'monchhichi',NULL,NULL,NULL),(103,'Mère Noël','2000-12-25','2017-05-20','P1150099-min.JPG','2017-05-20',0,'monchhichi',NULL,NULL,NULL),(104,'Christophe Maé','2000-12-25','2017-05-20','P1150162-min.JPG','2017-05-20',0,'monchhichi',NULL,NULL,NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productstags`
--

DROP TABLE IF EXISTS `productstags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productstags` (
  `productId` int(11) NOT NULL,
  `tagId` int(11) NOT NULL,
  PRIMARY KEY (`productId`,`tagId`),
  KEY `IDX_E665748236799605` (`productId`),
  KEY `IDX_E66574826F16ADDC` (`tagId`),
  CONSTRAINT `FK_E66574826F16ADDC` FOREIGN KEY (`tagId`) REFERENCES `tag` (`id`),
  CONSTRAINT `FK_E665748236799605` FOREIGN KEY (`productId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productstags`
--

LOCK TABLES `productstags` WRITE;
/*!40000 ALTER TABLE `productstags` DISABLE KEYS */;
INSERT INTO `productstags` VALUES (1,3),(1,13),(1,17),(2,12),(2,17),(3,12),(3,17),(4,12),(4,17),(5,12),(5,17),(6,12),(6,17),(7,12),(7,17),(8,12),(8,17),(9,12),(9,17),(10,6),(10,13),(11,3),(11,13),(11,17),(12,3),(12,13),(12,17),(13,3),(13,13),(13,18),(14,3),(14,13),(14,25),(15,3),(15,13),(15,25),(16,3),(16,13),(16,25),(17,3),(17,13),(17,25),(18,3),(18,13),(18,25),(19,3),(19,13),(19,25),(20,12),(21,12),(24,1),(24,11),(24,13),(24,20),(25,1),(25,11),(25,13),(25,17),(26,1),(26,11),(26,13),(26,17),(27,1),(27,11),(27,13),(27,17),(28,1),(28,11),(28,13),(28,17),(29,1),(29,11),(29,13),(29,17),(30,1),(30,11),(30,13),(30,17),(31,1),(31,11),(31,13),(31,17),(32,1),(32,11),(32,13),(32,17),(33,1),(33,11),(33,13),(34,1),(34,11),(34,13),(35,1),(35,11),(35,13),(35,29),(36,1),(36,11),(36,13),(37,1),(37,11),(37,13),(38,1),(38,11),(38,13),(38,29),(39,1),(39,11),(39,13),(39,29),(40,1),(40,11),(40,13),(41,1),(41,11),(41,13),(42,1),(42,11),(42,13),(43,9),(43,12),(43,13),(44,8),(44,9),(44,12),(44,13),(45,6),(45,13),(46,6),(46,13),(47,3),(47,13),(47,21),(48,3),(48,8),(48,13),(49,3),(49,13),(49,21),(50,3),(50,13),(50,21),(51,3),(51,13),(51,21),(52,1),(52,3),(52,7),(52,8),(52,13),(53,3),(53,13),(53,17),(54,3),(54,13),(55,3),(55,13),(56,3),(56,13),(56,18),(56,24),(57,3),(57,13),(57,22),(58,3),(58,13),(58,22),(59,3),(59,13),(60,3),(60,13),(60,17),(61,3),(61,13),(61,17),(62,3),(62,13),(62,17),(63,3),(63,13),(63,17),(64,3),(64,13),(64,20),(65,1),(65,11),(65,13),(65,19),(66,1),(66,11),(66,13),(66,19),(67,3),(67,13),(67,17),(68,3),(68,13),(68,17),(69,3),(69,13),(69,18),(70,3),(70,13),(70,18),(70,24),(71,3),(71,13),(71,18),(71,24),(72,3),(72,13),(72,17),(73,3),(73,13),(73,17),(74,3),(74,13),(75,3),(75,13),(75,18),(76,3),(76,13),(76,18),(77,3),(77,13),(77,24),(78,3),(78,13),(78,24),(79,3),(79,13),(79,24),(80,3),(80,13),(80,24),(81,3),(81,13),(81,24),(82,3),(82,13),(82,22),(83,12),(84,12),(85,12),(85,27),(85,28),(86,12),(87,12),(87,17),(88,12),(88,17),(90,12),(91,12),(91,17),(92,12),(92,17),(93,12),(93,17),(94,12),(94,17),(95,12),(95,17),(96,12),(96,17),(97,12),(97,17),(98,12),(98,17),(99,12),(99,17),(100,12),(100,17),(101,12),(101,17),(102,3),(102,13),(102,16),(102,29),(103,3),(103,13),(103,20),(103,29),(104,3),(104,13),(104,16),(104,29);
/*!40000 ALTER TABLE `productstags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sanitizeName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'7.5cm','7-5cm'),(2,'12cm','12cm'),(3,'20cm','20cm'),(4,'26cm','26cm'),(5,'40cm','40cm'),(6,'80cm','80cm'),(7,'Bebichhichi','bebichhichi'),(8,'Coffret','box'),(9,'Tirelire','moneybox'),(10,'Fève','charm'),(11,'Porte-clés','keychain'),(12,'Plastique','plastic'),(13,'Peluche','plush'),(14,'Grand-Parent','grand-parent'),(15,'Coloré','colorfuls'),(16,'Habits ajoutés','added-clothes'),(17,'Faune & flore','fauna-and-flora'),(18,'Corps dur','hard-body'),(19,'Monde','world'),(20,'Noël','christmas'),(21,'Pâtissier','pastry-chef'),(22,'Sportif','athletic'),(23,'Texto','text'),(24,'Employé','worker'),(25,'Rody','rody'),(27,'Deluxe','deluxe'),(28,'Monument','monument'),(29,'Les Premiers','the-first-ones');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-02 21:25:54
