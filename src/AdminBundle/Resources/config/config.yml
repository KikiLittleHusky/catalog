easy_admin:
    site_name: 'Cat(alog)'
    list:
        title: 'Liste des %%entity_label%%'
        actions:
            - { name: 'show', icon: 'eye', label: '' }
            - { name: 'edit', icon: 'pencil-square-o', label: '' }
            - { name: 'delete', icon: 'trash-o', label: '' }
    show:
        title: '%%entity_label%% (#%%entity_id%%)'
    edit:
        title: "Edition d'un %%entity_label%%"
    new:
        title: "Création d'un %%entity_label%%"
    entities:
        Tag:
            class: CatBundle\Entity\Tag
            label: 'Tags'
        Monchhichi:
            class: CatBundle\Entity\Monchhichi
            list:
                fields:
                    - { property: 'name', label: 'Nom', sortable: true }
                    - { property: 'purchaseDate', label: "Date d'achat", sortable: true }
                    - { property: 'creationDate', label: 'Date de création' }
                    - { property: 'image', type: 'image', base_path: %app.path.monchhichi_images% }
                    - { property: 'updatedAt', label: 'Date de mise à jour' }
                    - { property: 'wishlist', label: 'Wishlist', type: 'boolean', sortable: true  }
                    - { property: 'purchasePrice', label: "Prix d'achat", type: 'decimal', sortable: true }
                    - { property: 'sellingPrice', label: 'Prix de vente', type: 'decimal', sortable: true }
                    - { property: 'tags', label: 'Tags' }
            show:
                fields:
                    - { property: 'name', label: 'Nom' }
                    - { property: 'purchaseDate', label: "Date d'achat" }
                    - { property: 'creationDate', label: 'Date de création' }
                    - { property: 'image', type: 'image', base_path: %app.path.monchhichi_images% }
                    - { property: 'updatedAt', label: 'Date de mise à jour' }
                    - { property: 'wishlist', label: 'Wishlist', type: 'boolean'  }
                    - { property: 'purchasePrice', label: "Prix d'achat", type: 'decimal' }
                    - { property: 'sellingPrice', label: 'Prix de vente', type: 'decimal' }
                    - { property: 'tags', label: 'Tags' }
            form:
                fields:
                    - { property: 'name', label: 'Nom' }
                    - { property: 'purchaseDate', label: "Date d'achat", type: 'date', type_options: { widget: 'single_text' } }
                    - { property: 'creationDate', label: 'Date de création', type: 'date', type_options: { widget: 'single_text' } }
                    - { property: 'imageFile', type: 'vich_image' }
                    - { property: 'updatedAt', label: 'Date de mise à jour', type: 'date', type_options: { widget: 'single_text' } }
                    - { property: 'wishlist', label: 'Wishlist' }
                    - { property: 'purchasePrice', label: "Prix d'achat" }
                    - { property: 'sellingPrice', label: 'Prix de vente' }
                    - { property: 'tags', label: 'Tags', type_options: { expanded: true, multiple: true } }
        Nanoblock:
            class: CatBundle\Entity\Nanoblock
            list:
                fields:
                    - { property: 'name', label: 'Nom', sortable: true }
                    - { property: 'purchaseDate', label: "Date d'achat", sortable: true }
                    - { property: 'creationDate', label: 'Date de création' }
                    - { property: 'image', type: 'image', base_path: %app.path.nanoblock_images% }
                    - { property: 'updatedAt', label: 'Date de mise à jour' }
                    - { property: 'wishlist', label: 'Wishlist', type: 'boolean', sortable: true  }
                    - { property: 'nbPieces', label: 'Nombre de pièces', type: 'integer', sortable: true  }
                    - { property: 'tags', label: 'Tags' }
            show:
                fields:
                    - { property: 'name', label: 'Nom' }
                    - { property: 'purchaseDate', label: "Date d'achat" }
                    - { property: 'creationDate', label: 'Date de création' }
                    - { property: 'image', type: 'image', base_path: %app.path.nanoblock_images% }
                    - { property: 'updatedAt', label: 'Date de mise à jour' }
                    - { property: 'wishlist', label: 'Wishlist', type: 'boolean'  }
                    - { property: 'nbPieces', label: 'Nombre de pièces', type: 'integer', sortable: true  }
                    - { property: 'tags', label: 'Tags' }
            form:
                fields:
                    - { property: 'name', label: 'Nom' }
                    - { property: 'purchaseDate', label: "Date d'achat", type: 'date', type_options: { widget: 'single_text' } }
                    - { property: 'creationDate', label: 'Date de création', type: 'date', type_options: { widget: 'single_text' } }
                    - { property: 'imageFile', type: 'vich_image' }
                    - { property: 'updatedAt', label: 'Date de mise à jour', type: 'date', type_options: { widget: 'single_text' } }
                    - { property: 'wishlist', label: 'Wishlist' }
                    - { property: 'nbPieces', label: 'Nombre de pièces', type: 'integer', sortable: true  }
                    - { property: 'tags', label: 'Tags', type_options: { expanded: true, multiple: true }}
    design:
        menu:
            - label: 'Produits'
              icon: 'database'
              children:
                - {  entity: 'Monchhichi', label: 'Monchhichi', icon: 'heart' }
                - {  entity: 'Nanoblock', label: 'Nanoblock', icon: 'cubes' }
            - { entity: 'Tag', label: 'Tags', icon: 'tags' }
        color_scheme: 'light'
#        assets:
#            favicon: '/assets/backend/favicon.png'

vich_uploader:
    db_driver: orm
    mappings:
        monchhichi_images:
            uri_prefix:         %app.path.monchhichi_images%
            upload_destination: %kernel.root_dir%/../web/uploads/images/monchhichi
        nanoblock_images:
            uri_prefix:         %app.path.nanoblock_images%
            upload_destination: %kernel.root_dir%/../web/uploads/images/nanoblock