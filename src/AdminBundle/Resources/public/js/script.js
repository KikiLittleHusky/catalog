;(function () {

    'use strict';

    var generateCharts  = function(){
        var graphs = {};
        $('canvas.graph-line').each(function(k){
            var el = $(this),
                datasets = el.data('datasets'),
                labels = el.data('labels'),
                data = [],
                graphId = el.attr('id');
                data['datasets'] = [];
            if (datasets !== undefined) {
                for (var i = 0; i < datasets.length; i++) {
                    var newDataset = {
                        label: datasets[i]['label'],
                        backgroundColor: datasets[i]['backgroundColor'],
                        borderColor: datasets[i]['borderColor'],
                        borderWidth: datasets[i]['borderWidth'],
                        data: datasets[i]['data'],
                        fill: datasets[i]['fill']
                    };
                    // You add the newly created dataset to the list of `data`
                    data.datasets.push(newDataset);
                }


                graphs[graphId] = new Chart(el, {
                    type: 'line',
                    labels: labels,
                    data: data,
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });


                // You update the chart to take into account the new dataset
                //graphs[graphId].update();
            }
        });

    };

    $(window).ready(function(){
        generateCharts();
    });
}());