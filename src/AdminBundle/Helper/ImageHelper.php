<?php

namespace AdminBundle\Helper;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Process;

class ImageHelper
{
    private $configurationDirectories = [
        'monchhichi' => [
            'productminiatureforadmin',
            'productminiature_s',
            'productminiature_m',
            'productminiature_l',
            'productminiature_xl',
            'productminiature_xxl',
            'productfull'
        ]
        , 'nanoblock' => [
            'productminiatureforadmin',
            'productminiature_s',
            'productminiature_m',
            'productminiature_l',
            'productminiature_xl',
            'productminiature_xxl',
            'productfull'
        ]
        , 'funkopop' => [
            'productminiatureforadmin',
            'productminiature_s',
            'productminiature_m',
            'productminiature_l',
            'productminiature_xl',
            'productminiature_xxl',
            'productfull'
        ]
        , 'minis' => [
            'productminiatureforadmin',
            'productminiature_s',
            'productminiature_m',
            'productminiature_l',
            'productminiature_xl',
            'productminiature_xxl',
            'productfull'
        ]
        , 'medal' => [
            'productminiatureforadmin',
            'productminiature_s',
            'productminiature_m',
            'productminiature_l',
            'productminiature_xl',
            'productminiature_xxl',
            'productfull'
        ]
        , 'catalog' => [
            'catalogfull', 'catalogmin'
        ]
        , 'catalogBundle' => [
            'catbundlefull', 'catbundlemin'
        ]
    ];

    /** @var  Container */
    public $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function warmupImageCache($imagePath)
    {
        $webPath       = str_replace('/app', '', $this->container->getParameter('kernel.root_dir')) . '/web';
        $fullImagePath = $webPath . $imagePath;
        var_dump($fullImagePath);
        if ($imagePath === null || !file_exists($fullImagePath)) {
            throw new NotFoundHttpException('Erreur dans la console [RR843].');
        }

        $path = ($this->container->getParameter('host') == 'catalog.localhost')
            ? '/var/www/develop/catalog'
            : '/var/www/catalog';

        $process = new Process('cd ' . $path);
        $process->run();
        foreach ($this->configurationDirectories as $type => $filters) {
            if (strpos($imagePath, $type)) {
                foreach ($filters as $filter) {
                    $process->setCommandLine(
                        'cd ' . $path
                        . ' && php app/console liip:imagine:cache:remove ' . $imagePath . ' --filter=' . $filter
                    );
                    $process->run();
                    $process->setCommandLine(
                        'cd ' . $path
                        . ' && php app/console liip:imagine:cache:resolve ' . $imagePath . ' --filter=' . $filter
                    );
                    $process->run();
                }
                break;
            }
        }
    }
}
