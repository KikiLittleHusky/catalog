<?php
namespace AdminBundle\Controller;

use AdminBundle\Helper\ImageHelper;
use CatBundle\Entity\AbstractProduct;
use CatBundle\Entity\Tag;
use CatBundle\Repository\TagRepository;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;


use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use JavierEguiluz\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use JavierEguiluz\Bundle\EasyAdminBundle\Exception\EntityRemoveException;
use JavierEguiluz\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use JavierEguiluz\Bundle\EasyAdminBundle\Exception\NoEntitiesConfiguredException;
use JavierEguiluz\Bundle\EasyAdminBundle\Exception\UndefinedEntityException;
use JavierEguiluz\Bundle\EasyAdminBundle\Form\Util\LegacyFormHelper;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Templating\Helper\ImagineHelper;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

class AdminController extends BaseAdminController
{
    protected function createNewForm($entity, array $entityProperties)
    {
        if ($entity instanceof AbstractProduct) {
            if (method_exists($this, $customMethodName = 'create'.$this->entity['name'].'EntityForm')) {
                $form = $this->{$customMethodName}($entity, $entityProperties, 'new');
                if (!$form instanceof FormInterface) {
                    throw new \UnexpectedValueException(sprintf(
                        'The "%s" method must return a FormInterface, "%s" given.',
                        $customMethodName,
                        is_object($form) ? get_class($form) : gettype($form)
                    ));
                }
                return $form;
            }

            $methodName = str_replace('<EntityName>', $this->entity['name'], 'create<EntityName>EntityFormBuilder');
            if (!is_callable(array($this, $methodName))) {
                $methodName = str_replace('<EntityName>', '', 'create<EntityName>EntityFormBuilder');
            }
            $formBuilder = call_user_func_array(array($this, $methodName), array($entity, 'new'));

            if (!$formBuilder instanceof FormBuilderInterface) {
                throw new \UnexpectedValueException(sprintf(
                    'The "%s" method must return a FormBuilderInterface, "%s" given.',
                    'createEntityForm',
                    is_object($formBuilder) ? get_class($formBuilder) : gettype($formBuilder)
                ));
            }
            if ($formBuilder->has('tags')) {
                $formBuilder->remove('tags');
                /** @var TagRepository $tagRepo */
                $tagRepo = $this->getDoctrine()->getRepository('CatBundle:Tag');
                $tags = $tagRepo->getTagsByCatalog($entity->getSanitizeName());
                $formBuilder->add('tags', 'entity', array(
                    // *this line is important*
                    'choices_as_values' => true,
                    'choices'  => $tags,
                    'class'    => 'CatBundle:Tag',
                    'expanded' => true,
                    'multiple' => true,
                ));
            }
            return $formBuilder->getForm();
        } else {
            return parent::createNewForm($entity, $entityProperties);
        }
    }

    /**
     * Surcharge du formulaire d'édition pour filtrer la liste des tags en fonction
     * du Catalog
     * @param object $entity
     * @param array $entityProperties
     * @return Form|FormInterface
     */
    protected function createEditForm($entity, array $entityProperties)
    {
        if ($entity instanceof AbstractProduct) {
            if (method_exists($this, $customMethodName = 'create'.$this->entity['name'].'EntityForm')) {
                $form = $this->{$customMethodName}($entity, $entityProperties, 'edit');
                if (!$form instanceof FormInterface) {
                    throw new \UnexpectedValueException(sprintf(
                        'The "%s" method must return a FormInterface, "%s" given.',
                        $customMethodName,
                        is_object($form) ? get_class($form) : gettype($form)
                    ));
                }
                return $form;
            }

            $methodName = str_replace('<EntityName>', $this->entity['name'], 'create<EntityName>EntityFormBuilder');
            if (!is_callable(array($this, $methodName))) {
                $methodName = str_replace('<EntityName>', '', 'create<EntityName>EntityFormBuilder');
            }
            $formBuilder = call_user_func_array(array($this, $methodName), array($entity, 'edit'));

            if (!$formBuilder instanceof FormBuilderInterface) {
                throw new \UnexpectedValueException(sprintf(
                    'The "%s" method must return a FormBuilderInterface, "%s" given.',
                    'createEntityForm',
                    is_object($formBuilder) ? get_class($formBuilder) : gettype($formBuilder)
                ));
            }
            if ($formBuilder->has('tags')) {
                $formBuilder->remove('tags');
                /** @var TagRepository $tagRepo */
                $tagRepo = $this->getDoctrine()->getRepository('CatBundle:Tag');
                $tags = $tagRepo->getTagsByCatalog($entity->getSanitizeName());
                $formBuilder->add('tags', 'entity', array(
                    // *this line is important*
                    'choices_as_values' => true,
                    'choices'  => $tags,
                    'class'    => 'CatBundle:Tag',
                    'expanded' => true,
                    'multiple' => true,
                ));
            }
            return $formBuilder->getForm();
        } else {
            return parent::createEditForm($entity, $entityProperties);
        }
    }

    /**
     */
    public function rotateImageAction(Request $request)
    {
        $projectPath = ($this->container->getParameter('host') == 'catalog.localhost')
            ? '/var/www/develop/catalog'
            : '/var/www/catalog';
        $fullImgFile = $projectPath . '/web' . $request->request->get('imgPath');
        $imgFile = $request->request->get('imgPath');
        $rotation = $request->request->get('rotation');

        // Load
        $source = null;
        $ext = pathinfo($fullImgFile, PATHINFO_EXTENSION);
        if (in_array($ext, ['jpeg', 'jpg'])) {
            $source = imagecreatefromjpeg($fullImgFile);
        } elseif ($ext === 'png') {
            $source = imagecreatefrompng($fullImgFile);
        } elseif ($ext === 'gif') {
            $source = imagecreatefromgif($fullImgFile);
        }
        // Rotate
        $rotate = imagerotate($source, $rotation, 0);
        // Output
        imagejpeg($rotate, $fullImgFile);
        // Free the memory
        imagedestroy($source);
        imagedestroy($rotate);

        /** @var ImageHelper $imageHelper */
        $imageHelper = $this->container->get('cat.helper_image');
        $imageHelper->warmupImageCache($imgFile);


        return $this->redirect($request->headers->get('referer'));
    }
}
