<?php
namespace AdminBundle\Controller;

use CatBundle\Entity\Catalog;
use CatBundle\Helper\StatsHelper;
use CatBundle\Repository\CatalogRepository;
use CatBundle\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontAdminController extends Controller
{
    /**
     * Retourne les chiffres clés (quantité, montant des achats et moyenne du prix unitaire)
     * @return Response
     */
    public function statsKeyAction()
    {
        /** @var StatsHelper $statsHelper */
        $statsHelper = $this->get('cat.helper_stats');

        return $this->render('AdminBundle:Stats:main.html.twig', [
            'stats' => $statsHelper->getBackStatistics()
        ]);
    }

    /**
     * @TODO
     */
    public function statsGraphAction()
    {
        /** @var StatsHelper $statsHelper */
        $statsHelper = $this->get('cat.helper_stats');

        return $this->render('AdminBundle:Stats:graph.html.twig', [
            'stats' => $statsHelper->getBackGraphStatistics()
        ]);
    }

}
