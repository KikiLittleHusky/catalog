<?php
/**
 * Created by PhpStorm.
 * User: Rubi
 * Date: 28/01/2017
 * Time: 09:35
 */

namespace CatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="CatBundle\Repository\TagRepository")
 * @ORM\Table(name="tag")
 */
class Tag
{
    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, name="label")
     * @var string
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=100, name="sanitizeName")
     * @var string
     */
    private $sanitizeName;

    /**
     * @ORM\ManyToMany(targetEntity="CatBundle\Entity\AbstractProduct", mappedBy="tags")
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity="CatBundle\Entity\Catalog", mappedBy="tags")
     */
    private $catalogs;

    function __construct()
    {
        $this->id           = null;
        $this->label        = null;
        $this->sanitizeName = null;
        $this->products     = new ArrayCollection();
        $this->catalogs     = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->label;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Tag
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param $sanitizeName
     * @return $this
     */
    public function setSanitizeName($sanitizeName)
    {
        $this->sanitizeName = $sanitizeName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getSanitizeName()
    {
        return $this->sanitizeName;
    }

    /**
     * @param AbstractProduct $product
     * @return $this
     */
    public function addProduct(AbstractProduct $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * @param AbstractProduct $product
     */
    public function removeProduct(AbstractProduct $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Catalog $catalog
     * @return $this
     */
    public function addCatalog(Catalog $catalog)
    {
        $this->catalogs[] = $catalog;
        return $this;
    }

    /**
     * @param Catalog $catalog
     */
    public function removeCatalog(Catalog $catalog)
    {
        $this->catalogs->removeElement($catalog);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCatalogs()
    {
        return $this->catalogs;
    }
}
