<?php

namespace CatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="CatBundle\Repository\NanoblockRepository")
 * @ORM\Table(name="nanoblock")
 * @Vich\Uploadable
 */
class Nanoblock extends AbstractProduct
{
    public function getClassForFront()
    {
        return 'Nanoblock';
    }
    public function getSanitizeName()
    {
        return 'nanoblock';
    }
    public function getImageRepository(){
        return 'nanoblock_images';
    }

    /**
     * @ORM\Column(type="integer", name="nbPieces")
     */
    private $nbPieces;

    /**
     * @Vich\UploadableField(mapping="nanoblock_images", fileNameProperty="image", nullable=true)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="date", name="updatedAt")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", name="isAssembled")
     */
    private $isAssembled;

    function __construct()
    {
        parent::__construct();
        $this->nbPieces     = null;
        $this->imageFile    = null;
        $this->updatedAt    = new \DateTime('now');
        $this->isAssembled  = false;
    }

    /**
     * Set nbPieces
     *
     * @param integer $nbPieces
     * @return Nanoblock
     */
    public function setNbPieces($nbPieces)
    {
        $this->nbPieces = $nbPieces;

        return $this;
    }

    /**
     * Get nbPieces
     *
     * @return integer 
     */
    public function getNbPieces()
    {
        return $this->nbPieces;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getIsAssembled()
    {
        return $this->isAssembled;
    }

    /**
     * @param $isAssembled
     * @return $this
     */
    public function setIsAssembled($isAssembled)
    {
        $this->isAssembled = $isAssembled;
        return $this;
    }
}
