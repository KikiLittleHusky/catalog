<?php

namespace CatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="CatBundle\Repository\FunkopopRepository")
 * @ORM\Table(name="funkopop")
 * @Vich\Uploadable
 */
class Funkopop extends AbstractProduct
{
    public function getClassForFront()
    {
        return 'Funkopop';
    }
    public function getSanitizeName()
    {
        return 'funkopop';
    }
    public function getImageRepository(){
        return 'funkopop_images';
    }

    /**
     * @ORM\Column(type="decimal", scale=2, name="sellingPrice", nullable=true)
     */
    private $sellingPrice;

    /**
     * @Vich\UploadableField(mapping="funkopop_images", fileNameProperty="image", nullable=true)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="date", name="updatedAt")
     */
    private $updatedAt;

    function __construct()
    {
        parent::__construct();
        $this->sellingPrice  = null;
        $this->imageFile     = null;
        $this->updatedAt     = new \DateTime('now');;
    }

    /**
     * @param $sellingPrice
     * @return $this
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;
        return $this;
    }

    /**
     * Get sellingPrice
     *
     * @return string
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
