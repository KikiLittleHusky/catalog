<?php

namespace CatBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table("product")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="productType", type="string")
 * @ORM\DiscriminatorMap({
 *      "monchhichi"    = "Monchhichi",
 *      "nanoblock"     = "Nanoblock",
 *      "funkopop"      = "Funkopop",
 *      "minis"         = "Minis",
 *      "medal"         = "Medal",
 * })
 * @property int $id
 * @Vich\Uploadable
 */
abstract class AbstractProduct
{
    abstract function getClassForFront();
    abstract function getSanitizeName();
    abstract function getImageRepository();

    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, name="name")
     */
    protected $name;

    /**
     * @ORM\Column(type="date", name="purchaseDate", nullable=true)
     */
    protected $purchaseDate;

    /**
     * @ORM\Column(type="decimal", scale=2, name="purchasePrice", nullable=true)
     */
    private $purchasePrice;

    /**
     * @ORM\Column(type="date", name="creationDate")
     */
    protected $creationDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", name="wishlist")
     */
    protected $wishlist;

    /**
     * @ORM\Column(type="boolean", name="ordered")
     */
    protected $ordered;

    /**
     * @ORM\ManyToMany(targetEntity="CatBundle\Entity\Tag", cascade={"persist"}, inversedBy="products")
     * @ORM\JoinTable(name="productstags",
     *  joinColumns={@ORM\JoinColumn(name="productId", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="tagId", referencedColumnName="id")}
     * )
     */
    protected $tags;

    function __construct()
    {
        $this->id            = null;
        $this->name          = null;
        $this->purchaseDate  = null;
        $this->purchasePrice = null;
        $this->creationDate  = new \DateTime('now');
        $this->image         = null;
        $this->wishlist      = false;
        $this->ordered       = false;
        $this->tags          = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name . '(#' . $this->id . ')';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $purchaseDate
     * @return $this
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime 
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * Set purchasePrice
     *
     * @param string $purchasePrice
     * @return Monchhichi
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return string
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param $creationDate
     * @return $this
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param $wishlist
     * @return $this
     */
    public function setWishlist($wishlist)
    {
        $this->wishlist = $wishlist;

        return $this;
    }

    /**
     * Get wishlist
     *
     * @return boolean 
     */
    public function getWishlist()
    {
        return $this->wishlist;
    }

    /**
     * @param $ordered
     * @return $this
     */
    public function setOrdered($ordered)
    {
        $this->ordered = $ordered;

        return $this;
    }

    /**
     * Get ordered
     *
     * @return boolean
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
}
