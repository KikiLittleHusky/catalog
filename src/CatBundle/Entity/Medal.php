<?php

namespace CatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="CatBundle\Repository\MedalRepository")
 * @ORM\Table(name="medal")
 * @Vich\Uploadable
 */
class Medal extends AbstractProduct
{
    public function getClassForFront()
    {
        return 'Médailles';
    }
    public function getSanitizeName()
    {
        return 'medal';
    }
    public function getImageRepository(){
        return 'medal_images';
    }

    /**
     * @ORM\Column(type="integer", name="annee", nullable=true)
     */
    private $annee;

    /**
     * @ORM\Column(type="decimal", scale=2, name="purchasePrice", nullable=true)
     */
    private $purchasePrice;

    /**
     * @ORM\Column(type="decimal", scale=2, name="sellingPrice", nullable=true)
     */
    private $sellingPrice;

    /**
     * @Vich\UploadableField(mapping="medal_images", fileNameProperty="image", nullable=true)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="date", name="updatedAt")
     */
    private $updatedAt;

    function __construct()
    {
        parent::__construct();
        $this->purchasePrice = 2;
        $this->sellingPrice  = null;
        $this->imageFile     = null;
        $this->annee         = null;
        $this->updatedAt     = new \DateTime('now');;
    }

    /**
     * @param $sellingPrice
     * @return $this
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;
        return $this;
    }

    /**
     * Get sellingPrice
     *
     * @return string
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param $annee
     * @return $this
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;
        return $this;
    }
}
