<?php
namespace CatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="CatBundle\Repository\MonchhichiRepository")
 * @ORM\Table(name="monchhichi")
 * @Vich\Uploadable
 */
class Monchhichi extends AbstractProduct
{
    public function getClassForFront()
    {
        return 'Kiki';
    }
    public function getSanitizeName()
    {
        return 'kiki';
    }
    public function getImageRepository()
    {
        return 'monchhichi_images';
    }

    /**
     * @ORM\Column(type="decimal", scale=2, name="sellingPrice", nullable=true)
     */
    private $sellingPrice;

    /**
     * @Vich\UploadableField(mapping="monchhichi_images", fileNameProperty="image", nullable=true)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="date", name="updatedAt")
     */
    private $updatedAt;

    /**
     * constructeur
     */
    function __construct()
    {
        parent::__construct();
        $this->sellingPrice     = null;
        $this->imageFile        = null;
        $this->updatedAt        = new \DateTime('now');;
    }

    /**
     * Set sellingPrice
     *
     * @param string $sellingPrice
     * @return Monchhichi
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;

        return $this;
    }

    /**
     * Get sellingPrice
     *
     * @return string 
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
