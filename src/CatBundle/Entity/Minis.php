<?php

namespace CatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="CatBundle\Repository\MinisRepository")
 * @ORM\Table(name="minis")
 * @Vich\Uploadable
 */
class Minis extends AbstractProduct
{
    public function getClassForFront()
    {
        return 'Minis';
    }
    public function getSanitizeName()
    {
        return 'minis';
    }
    public function getImageRepository(){
        return 'minis_images';
    }

    /**
     * @Vich\UploadableField(mapping="minis_images", fileNameProperty="image", nullable=true)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="date", name="updatedAt")
     */
    private $updatedAt;

    function __construct()
    {
        parent::__construct();
        $this->imageFile    = null;
        $this->updatedAt    = new \DateTime('now');;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
