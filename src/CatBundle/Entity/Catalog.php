<?php
namespace CatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="CatBundle\Repository\CatalogRepository")
 * @ORM\Table(name="catalog")
 * @Vich\Uploadable
 */
class Catalog
{
    public function getImageRepository()
    {
        return 'catalog_images';
    }

    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, name="name")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="description", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="catalog_images", fileNameProperty="image", nullable=true)
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="date", name="updatedAt", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", name="activated")
     */
    private $activated;

    /**
     * @ORM\Column(type="string", name="sanitizeName", nullable=true)
     */
    private $sanitizeName;

    /**
     * @ORM\Column(type="string", name="className", nullable=true)
     */
    private $className;

    /**
     * @ORM\Column(type="string", name="titleSeo", nullable=true)
     */
    private $titleSeo;

    /**
     * @ORM\Column(type="string", name="descriptionSeo", nullable=true)
     */
    private $descriptionSeo;

    /**
     * @ORM\Column(type="boolean", name="hasWishilst")
     */
    private $hasWishlist;

    /**
     * @ORM\Column(type="boolean", name="hasAllList")
     */
    private $hasAllList;

    /**
     * @ORM\Column(type="boolean", name="hasTagList")
     */
    private $hasTagList;

    /**
     * @ORM\ManyToMany(targetEntity="CatBundle\Entity\Tag", cascade={"persist"}, inversedBy="catalogs")
     * @ORM\JoinTable(name="catalogstags",
     *  joinColumns={@ORM\JoinColumn(name="catalogId", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="tagId", referencedColumnName="id")}
     * )
     */
    private $tags;

    /**
     * @ORM\Column(type="string", name="htmlColor", nullable=true)
     */
    private $htmlColor;

    /**
     * constructeur
     */
    public function __construct()
    {
        $this->id               = null;
        $this->name             = null;
        $this->description      = null;
        $this->image            = null;
        $this->imageFile        = null;
        $this->updatedAt        = new \DateTime('now');
        $this->activated        = true;
        $this->sanitizeName     = null;
        $this->className        = null;
        $this->titleSeo         = null;
        $this->descriptionSeo   = null;
        $this->hasWishlist      = false;
        $this->hasAllList       = false;
        $this->hasTagList       = false;
        $this->tags             = new ArrayCollection();
        $this->htmlColor        = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setActivated($activated)
    {
        $this->activated = $activated;
    }

    public function getActivated()
    {
        return $this->activated;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get purchaseDate
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $sanitizeName
     * @return $this
     */
    public function setSanitizeName($sanitizeName)
    {
        $this->sanitizeName = $sanitizeName;

        return $this;
    }

    /**
     * @return null
     */
    public function getSanitizeName()
    {
        return $this->sanitizeName;
    }

    /**
     * @param $className
     * @return $this
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    /**
     * @return null
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Set titleSeo
     *
     * @param string $titleSeo
     * @return Catalog
     */
    public function setTitleSeo($titleSeo)
    {
        $this->titleSeo = $titleSeo;

        return $this;
    }

    /**
     * Get titleSeo
     *
     * @return string 
     */
    public function getTitleSeo()
    {
        return $this->titleSeo;
    }

    /**
     * Set descriptionSeo
     *
     * @param string $descriptionSeo
     * @return Catalog
     */
    public function setDescriptionSeo($descriptionSeo)
    {
        $this->descriptionSeo = $descriptionSeo;

        return $this;
    }

    /**
     * Get descriptionSeo
     *
     * @return string
     */
    public function getDescriptionSeo()
    {
        return $this->descriptionSeo;
    }

    /**
     * Set descriptionSeo
     *
     * @param string $description
     * @return Catalog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get descriptionSeo
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $hasWishlist
     */
    public function setHasWishlist($hasWishlist)
    {
        $this->hasWishlist = $hasWishlist;
    }

    /**
     * @return bool
     */
    public function getHasWishlist()
    {
        return $this->hasWishlist;
    }

    /**
     * @param $hasAllList
     */
    public function setHasAllList($hasAllList)
    {
        $this->hasAllList = $hasAllList;
    }

    /**
     * @return bool
     */
    public function getHasAllList()
    {
        return $this->hasAllList;
    }

    /**
     * @return mixed
     */
    public function getHasTagList()
    {
        return $this->hasTagList;
    }

    /**
     * @param $hasTagList
     * @return $this
     */
    public function setHasTagList($hasTagList)
    {
        $this->hasTagList = $hasTagList;
        return $this;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;
        return $this;
    }

    /**
     * Remove tags
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get htmlColor
     *
     * @return string
     */
    public function getHtmlColor()
    {
        return $this->htmlColor;
    }

    /**
     * @param $htmlColor
     * @return $this
     */
    public function setHtmlColor($htmlColor)
    {
        $this->htmlColor = $htmlColor;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
