<?php

namespace CatBundle\Helper;

use CatBundle\Entity\Catalog;
use CatBundle\Repository\CatalogRepository;
use CatBundle\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class StatsHelper
{
    const NUMBER_PRODUCT        = 'quantity';
    const TOTAL_AMOUNT_PURCHASE = 'amount';
    const AVERAGE_UNIT_PRICE    = 'averageUnit';

    /** @var  Container */
    public $container;

    /** @var  EntityManager */
    public $em;

    public function __construct($container, $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * Retourne les chiffres clés affichés sur le front
     * @param $catalogs
     * @return array
     */
    public function getStatistics($catalogs)
    {
        $stats = array();
        /** @var Catalog $catalog */
        foreach ($catalogs as $catalog){
            if ($this->container->get('cat.classchecker')->isCatalogClassExisting($catalog->getClassName())) {
                /** @var ProductRepository $productRepository */
                $productRepository = $this->em->getRepository('CatBundle:' . $catalog->getClassName());
                $stats[$catalog->getSanitizeName()]['total'] = $productRepository->countProduct(0);
                if ($catalog->getHasWishlist()) {
                    $stats[$catalog->getSanitizeName()]['wishlist'] = $productRepository->countProduct(1);
                }

                if (method_exists($productRepository, 'countNbPieces')) {
                    $stats[$catalog->getSanitizeName()]['countNbPieces'] = $productRepository->countNbPieces(0, 1);
                    if ($catalog->getHasWishlist()) {
                        $stats[$catalog->getSanitizeName()]['countNbPiecesToAssemble'] = $productRepository->countNbPieces(0, 0);
                    }
                }
            }
        }
        return $stats;
    }

    public function getLatestPurchases($catalogs)
    {
        $products = array();
        /** @var Catalog $catalog */
        foreach ($catalogs as $catalog){
            if ($this->container->get('cat.classchecker')->isCatalogClassExisting($catalog->getClassName())) {
                /** @var ProductRepository $productRepository */
                $productRepository = $this->em->getRepository('CatBundle:' . $catalog->getClassName());
                $products[$catalog->getSanitizeName()] = $productRepository->findLatestProducts(0);
            }
        }
        return $products;
    }


    /**
     * Retourne les chiffres clés affichés dans le back
     * @param $catalogs
     * @return array
     */
    public function getBackStatistics()
    {
        /** @var CatalogRepository $repository */
        $repository = $this->em->getRepository('CatBundle:Catalog');
        $catalogs   = $repository->findAll();

        $statistiques = array();
        /** @var Catalog $catalog */
        foreach ($catalogs as $catalog) {
            $sanitizedCatalog = $catalog->getSanitizeName();
            /** @var ProductRepository $productRepository */
            $productRepository = $this->em->getRepository('CatBundle:' . $catalog->getClassName());
            $statistiques[$sanitizedCatalog][StatsHelper::NUMBER_PRODUCT] =
                StatsHelper::getNumberOfProducts($productRepository);
            $statistiques[$sanitizedCatalog][StatsHelper::TOTAL_AMOUNT_PURCHASE] =
                StatsHelper::getPurchaseAmount($productRepository);
            $statistiques[$sanitizedCatalog][StatsHelper::AVERAGE_UNIT_PRICE] = StatsHelper::getAverageUnitPrice(
                $statistiques[$sanitizedCatalog][StatsHelper::TOTAL_AMOUNT_PURCHASE],
                $statistiques[$sanitizedCatalog][StatsHelper::NUMBER_PRODUCT]
            );
        }

        return StatsHelper::getGlobalStats($statistiques) + $statistiques;
    }

    /**
     * Retourne les statistiques utilisés dans les graphs du back
     */
    public function getBackGraphStatistics()
    {
        /** @var CatalogRepository $repository */
        $repository = $this->em->getRepository('CatBundle:Catalog');
        $catalogs   = $repository->findAll();

        $statistiques = array();
        /** @var Catalog $catalog */
        foreach ($catalogs as $catalog) {
            $sanitizedCatalog = $catalog->getSanitizeName();
            /** @var ProductRepository $productRepository */
            $productRepository = $this->em->getRepository('CatBundle:' . $catalog->getClassName());
            $statistiques[$sanitizedCatalog][StatsHelper::NUMBER_PRODUCT] =
                StatsHelper::getNumberOfProducts($productRepository);
            $statistiques[$sanitizedCatalog][StatsHelper::TOTAL_AMOUNT_PURCHASE] =
                StatsHelper::getPurchaseAmount($productRepository);
            $statistiques[$sanitizedCatalog][StatsHelper::AVERAGE_UNIT_PRICE] = StatsHelper::getAverageUnitPrice(
                $statistiques[$sanitizedCatalog][StatsHelper::TOTAL_AMOUNT_PURCHASE],
                $statistiques[$sanitizedCatalog][StatsHelper::NUMBER_PRODUCT]
            );
        }

        return StatsHelper::getGlobalStats($statistiques) + $statistiques;
    }

    /**
     *
     */
    private static function getNumberOfProducts(ProductRepository $productRepository, $period = null)
    {
        if ($period === null or true) {
            return $productRepository->countProduct(0);
        }
        return 0;
    }

    /**
     *
     */
    private static function getPurchaseAmount(ProductRepository $productRepository, $period = null)
    {
        if ($period === null or true) {
            return $productRepository->sumPurchaseAmount();
        }
        return 0;
    }

    /**
     *
     */
    private static function getAverageUnitPrice($amount, $quantity)
    {
        return (($amount * 100) / ($quantity * 100));
    }

    private static function getGlobalStats(array $statistiques)
    {
        $globalStats = [
            StatsHelper::NUMBER_PRODUCT => 0,
            StatsHelper::TOTAL_AMOUNT_PURCHASE => 0,
            StatsHelper::AVERAGE_UNIT_PRICE => 0,
        ];
        foreach ($statistiques as $stats) {
            $globalStats[StatsHelper::NUMBER_PRODUCT] += $stats[StatsHelper::NUMBER_PRODUCT];
            $globalStats[StatsHelper::TOTAL_AMOUNT_PURCHASE] += $stats[StatsHelper::TOTAL_AMOUNT_PURCHASE];
            $globalStats[StatsHelper::AVERAGE_UNIT_PRICE] += $stats[StatsHelper::AVERAGE_UNIT_PRICE];
        }
        return ['all' => $globalStats];
    }
}