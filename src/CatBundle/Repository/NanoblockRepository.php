<?php

namespace CatBundle\Repository;

use CatBundle\Entity\Nanoblock;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * NanoblockRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NanoblockRepository extends ProductRepository
{
    public function getTable ()
    {
        return 'Nanoblock';
    }

    public function countNbPieces($wishlist = 0, $isAssembled = 1)
    {
        if ($wishlist !== 0 && $wishlist !== 1 && $wishlist !== 2) {
            return new NotFoundHttpException('The value of wishlist must be 0 or 1');
        }
        if ($isAssembled !== 0 && $isAssembled !== 1 && $isAssembled !== 2) {
            return new NotFoundHttpException('The value of isAssembled must be 0 or 1');
        }

        $query = $this->createQueryBuilder('n');
        $query
            ->select('SUM(n.nbPieces)')
            ->andWhere('n.ordered = :ordered')
            ->setParameter('ordered', 0)
            ->andWhere('n.wishlist = :wishlist')
            ->setParameter('wishlist', $wishlist)
            ->andWhere('n.isAssembled = :isAssembled')
            ->setParameter('isAssembled', $isAssembled);

        return $query ->getQuery() ->getSingleScalarResult();
    }
}
