;(function () {
	
	'use strict';

   	// iPad and iPod detection
	var isiPad = function(){
		return (navigator.platform.indexOf("iPad") != -1);
	};

	var isiPhone = function(){
	    return (
			(navigator.platform.indexOf("iPhone") != -1) || 
			(navigator.platform.indexOf("iPod") != -1)
	    );
	};

	var fullHeight = function() {
		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function(){
			$('.js-fullheight').css('height', $(window).height());
		});
	};

	// Animations
	var contentWayPoint = function() {
		var i = 0;
		$('.animate-box').waypoint( function( direction ) {
			if( direction === 'down' && !$(this.element).hasClass('animated') ) {
				i++;
				$(this.element).addClass('item-animate');
				setTimeout(function(){
					$('body .animate-box.item-animate').each(function(k){
						var el = $(this);
						setTimeout( function () {
							var effect = el.data('animate-effect');
							if ( effect === 'fadeIn') {
								el.addClass('fadeIn animated');
							} else {
								el.addClass('fadeInUp animated');
							}

							el.removeClass('item-animate');
						},  k * 200, 'easeInOutExpo' );
					});
				}, 15);
			}
		} , { offset: '85%' } );
	};
	
	var counter = function() {
		$('.js-counter').countTo({
			formatter: function (value, options) {
	     	return value.toFixed(options.decimals);
	    	}
		});
	};

	var counterWayPoint = function() {
		if ($('#counter-animate').length > 0 ) {
			$('#counter-animate').waypoint( function( direction ) {

				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout( counter , 400);
					$(this.element).addClass('animated');

				}
			} , { offset: '90%' } );
		}
	};

    var responsiveImages = function() {
        $('.miniatureProduct').each(function(index, element){
            var srcAttr = 'data-src-xl',
                heightAttr = 'data-height-xl';

            if (window.matchMedia("(max-width: 700px)").matches) {
                srcAttr = 'data-src-m';
                heightAttr = 'data-height-m';
            } else if (window.matchMedia("(min-width: 1200px)").matches) {
                srcAttr = 'data-src-xl';
                heightAttr = 'data-height-xl';
            } else if (window.matchMedia("(min-width: 1000px)").matches) {
                srcAttr = 'data-src-l';
                heightAttr = 'data-height-l';
            } else if (window.matchMedia("(min-width: 701px)").matches) {
                srcAttr = 'data-src-xxl';
                heightAttr = 'data-height-xxl';
            }
            var src = element.getAttribute(srcAttr),
                height = element.getAttribute(heightAttr);

            $(element).css(
                {'background-image':'url(' + src + ')'
                    , 'height': height + 'px'
                    , 'background-repeat': 'no-repeat'
                    , 'background-position': 'center center'
                }
            );

        });
    };

    var burgerMenu  = function(){

    };

	// Document on load.
	$(function(){
		fullHeight();
		burgerMenu();
		counter();
		contentWayPoint();
counterWayPoint();
//		imgPopup();
        responsiveImages();
	});
    $(window).resize(function(){
        responsiveImages();
    });
}());