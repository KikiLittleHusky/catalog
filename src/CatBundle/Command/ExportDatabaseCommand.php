<?php
namespace CatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ExportDatabaseCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('cat:database:export')
            ->setDescription('Dump la base de données MySql.')
            ->setHelp('Dump la base de données MySql.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container  = $this->getContainer();
        $host       = $container->getParameter('database_host');
        $name       = $container->getParameter('database_name');
        $user       = $container->getParameter('database_user');
        $password   = $container->getParameter('database_password');

        $dumpFile = '/var/tmp/deploy/' . $name . date('YmdHis') . '.sql';

        $process = new Process(
            'mysqldump -u ' . $user
            . ' -p ' . $name
            . ' -h ' . $host
            . ' --password=' . $password
            . ' > ' . $dumpFile);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }else {
            echo $dumpFile;
        }
    }
}