<?php
namespace CatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Console\Input\ArrayInput;

class MediaCacheWarmupCommand extends ContainerAwareCommand
{
    private $configurationDirectories = array(
         'monchhichi'    => array(
             'args' => array('productminiatureforadmin', 'productminiature_s', 'productminiature_m', 'productminiature_l', 'productminiature_xl', 'productminiature_xxl', 'productfull')
             ,'pathParam' => 'app.path.monchhichi_images'
         )
        ,'nanoblock'    => array(
            'args' => array('productminiatureforadmin', 'productminiature_s', 'productminiature_m', 'productminiature_l', 'productminiature_xl', 'productminiature_xxl', 'productfull')
            ,'pathParam' => 'app.path.nanoblock_images'
        )
        ,'funkopop'    => array(
            'args' => array('productminiatureforadmin', 'productminiature_s', 'productminiature_m', 'productminiature_l', 'productminiature_xl', 'productminiature_xxl', 'productfull')
            ,'pathParam' => 'app.path.funkopop_images'
        )
        ,'minis'    => array(
            'args' => array('productminiatureforadmin', 'productminiature_s', 'productminiature_m', 'productminiature_l', 'productminiature_xl', 'productminiature_xxl', 'productfull')
            ,'pathParam' => 'app.path.minis_images'
        )
        ,'medal'    => array(
            'args' => array('productminiatureforadmin', 'productminiature_s', 'productminiature_m', 'productminiature_l', 'productminiature_xl', 'productminiature_xxl', 'productfull')
            ,'pathParam' => 'app.path.medal_images'
        )
        ,'catalog'      => array(
            'args' => array('catalogfull', 'catalogmin')
            ,'pathParam' => 'app.path.catalog_images'
        )
        ,'catalogBundle'      => array(
            'args' => array('catbundlefull', 'catbundlemin')
            ,'pathParam' => 'app.path.cat_images'
        )
    );

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('cat:media:cache:warmup')
            ->setDescription('Crée le cache des médias.')
            ->setHelp('Crée le cache des médias.')
            ->addOption('action', 'a', InputOption::VALUE_OPTIONAL, '0 (defaut) : supprime et recrée | 1 : crée | 2 : supprime')
            ->addOption('filter', 'f', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'Quel est le filtre à warmup ?')
            ->addOption('type', 't', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'Quel est le dossier à warmup ?')
            ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container  = $this->getContainer();
        $webPath    = str_replace('/app', '', $container->getParameter('kernel.root_dir')) . '/web';
        $typeCmd    = ($input->getOption('action') === null) ? '0' : $input->getOption('action');
        $filter     = $input->getOption('filter');
        $type       = $input->getOption('type');

        if ($typeCmd !== null && !in_array($typeCmd, array('0', '1', '2'))) {
            throw new NotFoundHttpException('Erreur dans la console [RR842].');
        }
        if ($filter !== null && !is_array($filter)) {
            throw new NotFoundHttpException('Erreur dans la console [RR843].');
        }
        if ($type !== null) {
            if (is_array($type)) {
                foreach ($type as $unType) {
                    if (!isset($this->configurationDirectories[$unType])){
                        throw new NotFoundHttpException('Le type d\'image "'. $unType .'" n\'est pas géré.');
                    }
                }
            } else {
                throw new NotFoundHttpException('Erreur dans la console [RR844].');
            }
        }

        $filesToWarmup = array();

        if (count($type) == 0 && count($filter) == 0) {
            $directories = array();
            $directories['monchhichi'] =
                $webPath . $container->getParameter(
                    $this->configurationDirectories['monchhichi']['pathParam']
                );
            $directories['nanoblock'] =
                $webPath . $container->getParameter(
                    $this->configurationDirectories['nanoblock']['pathParam']
                );
            $directories['funkopop'] =
                $webPath . $container->getParameter(
                    $this->configurationDirectories['funkopop']['pathParam']
                );
            $directories['minis'] =
                $webPath . $container->getParameter(
                    $this->configurationDirectories['minis']['pathParam']
                );
            $directories['medal'] =
                $webPath . $container->getParameter(
                    $this->configurationDirectories['medal']['pathParam']
                );
            $directories['catalog'] =
                $webPath . $container->getParameter(
                    $this->configurationDirectories['catalog']['pathParam']
                );
            $directories['catalogBundle'] =
                $webPath . $container->getParameter(
                    $this->configurationDirectories['catalogBundle']['pathParam']
                );

            foreach ($directories as $nom => $chemin) {
                $config = array();
                if (isset($this->configurationDirectories[$nom]) && isset($this->configurationDirectories[$nom]['args'])) {
                    $tmp = $this->configurationDirectories[$nom]['args'];
                    $filtres = '';
                    foreach ($tmp as $filter) {
                        $filtres .= ' --filters=' . $filter;
                    }
                    $config['args'] = $filtres;
                }

                $filesToWarmup[$nom] = array_merge(
                    $this->find_all_files($chemin, array())
                    , $config
                );
            }
        } else {
            if (count($filter) > 0 && count($type) > 0) {
//                $args = '';
//                foreach ($filter as $filtre) {
//                    $args .= ' --filters=' . $filtre;
//                }
//                $config['args'] = $args;
            } else if (count($type) > 0) {
                $directories = array();
                foreach ($type as $unType) {
                    if (isset($this->configurationDirectories[$unType]['pathParam'])) {
                        $directories[$unType] = $webPath . $container->getParameter(
                                $this->configurationDirectories[$unType]['pathParam']
                            );
                    } else if (isset($this->configurationDirectories[$unType]['path'])) {
                        $directories[$unType] = $webPath . $this->configurationDirectories[$unType]['pathParam'];
                    }
                }

                foreach ($directories as $nom => $chemin) {
                    $config = array();
                    if (isset($this->configurationDirectories[$nom]) && isset($this->configurationDirectories[$nom]['args'])) {
                        $tmp = $this->configurationDirectories[$nom]['args'];
                        $filtres = '';
                        foreach ($tmp as $filter) {
                            $filtres .= ' --filters=' . $filter;
                        }
                        $config['args'] = $filtres;
                    }

                    $filesToWarmup[$nom] = array_merge(
                        $this->find_all_files($chemin, array())
                        , $config
                    );
                }

            } else if (count($filter) > 0) {
                $args = '';
                foreach ($filter as $filtre) {
                    $args .= ' --filters=' . $filtre;
                }
                $config['args'] = $args;

                foreach ($this->configurationDirectories as $name => $configuration) {
                    if (isset($configuration['args'])) {
                        foreach ($filter as $oneFilter) {
                            $search = array_search($oneFilter, $configuration['args']);
                            if ($search !== false) {
                                $filesToWarmup[$name] = array_merge(
                                    $this->find_all_files(
                                        $webPath . $container->getParameter($configuration['pathParam'])
                                        , array()
                                    ), $config
                                );
                                break;
                            }
                        }
                    }
                }
            }
        }

        $path = ($this->getContainer()->getParameter('host') == 'catalog.localhost')
            ? '/var/www/develop/catalog'
            : '/var/www/catalog';

        $process = new Process('cd ' . $path);
        $process->run();
        foreach ($filesToWarmup as $type => $files) {
            $args = '';
            if (isset($files['args'])) {
                $args = $files['args'];
                unset($files['args']);
            }
            foreach ($files as $file) {
                $file = str_replace($path . '/web', '', $file);
                if ($typeCmd == 0 || $typeCmd == 2) {
                    $process->setCommandLine('php app/console liip:imagine:cache:remove ' . $file . ' ' . $args);
                    $process->run();
                }
                if ($typeCmd == 0 || $typeCmd == 1) {
                    $process->setCommandLine('php app/console liip:imagine:cache:resolve ' . $file . ' ' . $args);
                    $process->run();
                }
                $output->writeln('<comment>Génération des caches pour ' . $file . ' terminée.</comment>');
            }
        }
    }

    /**
     * @param $dir
     * @param $result
     * @return array
     */
    private function find_all_files($dir, $result)
    {
        $root = scandir($dir);
        foreach($root as $value)
        {
            if ($value === '.' || $value === '..') {
                continue;
            }
            if (is_file("$dir/$value")) {
                $result[] = "$dir/$value";
                continue;
            }
            foreach ($this->find_all_files("$dir/$value", $result) as $subvalue) {
                $result[] = $subvalue;
            }
        }
        return $result;
    }
}