<?php
namespace CatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Console\Input\ArrayInput;

class DeployDevToProdCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('cat:deploy-dev-to-prod')
            ->setDescription('Déploie la version de dev en prod.')
            ->setHelp('Déploie la version de dev en prod.')
            ->addArgument('copyBdd', InputArgument::OPTIONAL, 'Doit-on copier la BDD de Dev en Prod ? (true par défaut, false')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $copyBdd = ($input->getArgument('copyBdd') === false) ? false : true;
//
//        $currentDatabse = $this->getContainer()->getParameter('database_host');
//        if ($currentDatabse == 'devcatalog.create-it.solutions'
//            && file_exists('/var/www/devcatalog')
//        ) {
//            $process = new Process('cd /var/www/devcatalog');
//            $process->run();
//            if (file_exists('/var/www/catalog')) {
//                $output->writeln('<comment>Déplacement du dossier de Prod déjà existant.</comment>');
//                $process->setCommandLine('sudo mv /var/www/catalog /var/www/catalog-backup' . date('YmdHis'));
//                $process->run();
//            }
//
//            $output->writeln('<comment>Copie des fichiers.</comment>');
//            $process->setCommandLine('sudo cp -R /var/www/devcatalog /var/www/catalog');
//            $process->run();
//            $process->setCommandLine('sudo setfacl -Rm d:u:www-data:rwx,u:www-data:rwx,u:ssh-agent:rwx /var/www/catalog/');
//            $process->run();
//            $process->setCommandLine('sudo chown -R www-data:www-data /var/www/catalog/');
//            $process->run();
//
//            $output->writeln('<comment>Modification du fichier parameters.yml.</comment>');
//            file_put_contents(
//                '/var/www/catalog/app/config/parameters.yml',
//                file_get_contents('/var/www/catalog/app/config/parameters.yml.model')
//            );
//
//            if ($copyBdd) {
//                $output->writeln('<comment>Copie de la base de données de dev en Prod.</comment>');
//
//                $command = $this->getApplication()->find('cat:database:export');
//                $greetInput = new ArrayInput(array(''));
//                ob_start();
//                $returnCode = $command->run($greetInput, $output);
//                $outputText = ob_get_clean();
//
//                $output->writeln('<comment>Suppression du cache.</comment>');
//                $process->setCommandLine('cd /var/www/catalog/ && rm -rf app/cache*');
//                $process->run();
//                if($returnCode == 0) {
//                    $output->writeln('<question>Extraction de la BDD de dev : ' . $outputText . '</question>');
//                    $output->writeln('<question>Importation de la BDD en prod.</question>');
//                    $process->setCommandLine('cd /var/www/catalog/ &&  php app/console doctrine:database:import ' . $outputText);
//                    $process->run();
//                }
//            }
//
//            $output->writeln('<comment>Suppression du cache.</comment>');
//            $process->setCommandLine('cd /var/www/catalog/ && rm -rf app/cache*');
//            $process->run();
//            $output->writeln('<comment>Dump d\'assetic.</comment>');
//            $process->setCommandLine('cd /var/www/catalog/ && php app/console assetic:dump --env=prod');
//            $process->run();
//            $output->writeln('<comment>Install des assets.</comment>');
//            $process->setCommandLine('cd /var/www/catalog/ && php app/console assets:install');
//            $process->run();
//        } else {
//            echo "\n" . 'L\'environnement de Dev est mal configuré.' . "\n";
//        }
    }
}