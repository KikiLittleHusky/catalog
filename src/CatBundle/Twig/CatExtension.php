<?php

namespace CatBundle\Twig;
use CatBundle\Entity\AbstractProduct;
use CatBundle\Entity\Catalog;
use CatBundle\Repository\CatalogRepository;
use CatBundle\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Imagine\Gd\Imagine;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Templating\Helper\ImagineHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

class CatExtension extends \Twig_Extension
{
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->em        = $em;
    }

    public function getFunctions()
    {
        return array(
            'getTypeProduct'        => new \Twig_Function_Method($this, 'getClassName'),
            'getRouteProduct'       => new \Twig_Function_Method($this, 'getSanitizeName'),
            'getTopNavigation'      => new \Twig_Function_Method($this, 'getTopNavigation'),
            'getBottomNavigation'   => new \Twig_Function_Method($this, 'getBottomNavigation'),
            'getListTags'           => new \Twig_Function_Method($this, 'getListTags'),
            'isCurrentMenu'         => new \Twig_Function_Method($this, 'isCurrentMenu'),
            'getHeaderImage'        => new \Twig_Function_Method($this, 'getHeaderImage'),
            'getImageLink'          => new \Twig_Function_Method($this, 'getImageLink'),
        );
    }

    /**
     * Retourne le nom du produit pour le front
     * @param AbstractProduct $arguments
     * @return mixed
     */
    public function getClassName(AbstractProduct $arguments)
    {
        return $arguments->getClassForFront();
    }

    /**
     * Retourne le nom du produit en sanitize
     * @param AbstractProduct $arguments
     * @return mixed
     */
    public function getSanitizeName(AbstractProduct $arguments)
    {
        return $arguments->getSanitizeName();
    }

    /**
     * Retourne le chemin du dossier d'upload d'image
     * @param $arguments
     * @return string
     */
    private function getImageRepository($arguments)
    {
        if ($arguments instanceof AbstractProduct || $arguments instanceof Catalog) {
            $globals = $this->container->get('twig')->getGlobals();
            return $globals[$arguments->getImageRepository()] . '/';
        }
        return null;
    }

    /**
     * Retourne la liste des liens à afficher dans la navigation du header
     * @return string
     */
    public function getTopNavigation()
    {
        $pages = array();

        $twigService = $this->container->get('twig');
        // récupération des pages paramétrées au niveau du twig
        if (isset($twigService->getGlobals()['static_pages'])) {
            $globalPages = $twigService->getGlobals()['static_pages'];
            foreach ($globalPages as $page) {
                $pages[$page['route']] = $page['label'];
            }
        }

        /** @var CatalogRepository $catalogRepository */
        $catalogRepository = $this->em->getRepository('CatBundle:Catalog');
        $paramForCatalogs = $catalogRepository->findActivationsByCatalog();

        $collectionRoute      = $this->container->getParameter('group_pages.collection.route');
        $collectionLabelModel = $this->container->getParameter('group_pages.collection.label');
        $wishlistRoute        = $this->container->getParameter('group_pages.wishlist.route');
        $allListRouteModel    = $this->container->getParameter('group_pages.all_list.route');
        $wishlistLabelModel   = $this->container->getParameter('group_pages.wishlist.label');
        $allListLabelModel    = $this->container->getParameter('group_pages.all_list.label');

        foreach ($paramForCatalogs as $nameCatalog => $config) {
            $subpages = array();
            if ($config['collection']) {
                $labelCatalog = $config['name'];
                $collectionLabel = str_replace('#catalog#', $labelCatalog, $collectionLabelModel);
                if ($config['tagList']) {
                    $subpages[$collectionRoute . '/' . $nameCatalog] = $collectionLabel;
                }
                if ($config['allList']) {
                    $allListLabel = str_replace('#catalog#', $labelCatalog, $allListLabelModel);
                    $allListRoute = str_replace('#catalog#', $nameCatalog, $allListRouteModel);
                    $subpages[$allListRoute] = $allListLabel;
                }
                if ($config['wishlist']){
                    $wishlistLabel = str_replace('#catalog#', $labelCatalog, $wishlistLabelModel);
                    $subpages[$wishlistRoute . '/' . $nameCatalog] = $wishlistLabel;
                }
                $pages[$labelCatalog] = $subpages;
            }
        }
        return $pages;
    }

    /**
     * Retourne la liste des liens à afficher dans la navigation du footer
     * @return array
     */
    public function getBottomNavigation()
    {
        $pages = array();

        $twigService = $this->container->get('twig');
        // récupération des pages paramétrées au niveau du twig
        if (isset($twigService->getGlobals()['static_pages'])) {
            $globalPages = $twigService->getGlobals()['static_pages'];
            foreach ($globalPages as $page) {
                $pages[$page['route']] = $page['label'];
            }
        }

        // récupération des routes des Catalogs, des Wishlist et des AllListes qui sont activés
        $collectionRoute      = $this->container->getParameter('group_pages.collection.route');
        $wishlistRoute        = $this->container->getParameter('group_pages.wishlist.route');
        $allListRouteModel    = $this->container->getParameter('group_pages.all_list.route');
        $collectionLabelModel = $this->container->getParameter('group_pages.collection.label');
        $wishlistLabelModel   = $this->container->getParameter('group_pages.wishlist.label');
        $allListLabelModel    = $this->container->getParameter('group_pages.all_list.label');

        /** @var CatalogRepository $catalogRepository */
        $catalogRepository = $this->em->getRepository('CatBundle:Catalog');
        $paramForCatalogs = $catalogRepository->findActivationsByCatalog();

        foreach ($paramForCatalogs as $nameCatalog => $config) {
            $subpages = array();
            if ($config['collection']) {
                $labelCatalog = $config['name'];
                $collectionLabel = str_replace('#catalog#', $labelCatalog, $collectionLabelModel);
                if ($config['tagList']) {
                    $subpages[$collectionRoute . '/' . $nameCatalog] = $collectionLabel;
                }
                if ($config['allList']) {
                    $allListLabel = str_replace('#catalog#', $labelCatalog, $allListLabelModel);
                    $allListRoute = str_replace('#catalog#', $nameCatalog, $allListRouteModel);
                    $subpages[$allListRoute] = $allListLabel;
                }
                if ($config['wishlist']){
                    $wishlistLabel = str_replace('#catalog#', $labelCatalog, $wishlistLabelModel);
                    $subpages[$wishlistRoute . '/' . $nameCatalog] = $wishlistLabel;
                }
                $pages[$labelCatalog] = $subpages;
            }
        }

        return $pages;
    }


    /**
     * Retourne la liste des liens à afficher dans la navigation du footer
     * @return array
     */
    public function getListTags()
    {
        $pages = array();

        // récupération des routes des Catalogs, des Wishlist et des AllListes qui sont activés
        $tagRouteModel = $this->container->getParameter('group_pages.tag.route');
        $tagLabelModel = $this->container->getParameter('group_pages.tag.label');


        /** @var CatalogRepository $catalogRepository */
        $catalogRepository = $this->em->getRepository('CatBundle:Catalog');
        $paramForCatalogs = $catalogRepository->findActivationsByCatalog();

        foreach ($paramForCatalogs as $nameCatalog => $config) {
            if ($config['tagList']) {
                $subpages = array();
                $labelCatalog = $config['name'];
                if ($config['collection']) {
                    if ($this->container->get('cat.classchecker')->isCatalogClassExisting($config['className'])) {
                        /** @var ProductRepository $productRepository */
                        $productRepository = $this->em->getRepository('CatBundle:' . $config['className']);
                        $tags = $productRepository->countProductsByTagOrNot();
                        foreach ($tags as $tag) {
                            if ($tag['label'] !== null) {
                                $tagLabel = str_replace(array('#tag#', '#count#'), array($tag['label'], $tag[1]), $tagLabelModel);
                                $tagRoute = str_replace(array('#catalog#', '#tag#'), array($nameCatalog, $tag['sanitizeName']), $tagRouteModel);
                                $subpages[$tagRoute] = $tagLabel;
                            }
                        }
                        $pages[$labelCatalog] = $subpages;
                    }
                }
            }
        }
        return $pages;
    }

    public function isCurrentMenu($route, $current)
    {
        if ($route == $current){
            return true;
        } elseif (strpos($route, '/') === false){
            /** @var CatalogRepository $catalogRepository */
            $catalogRepository = $this->em->getRepository('CatBundle:Catalog');
            /** @var Catalog $catalog */
            $catalog = $catalogRepository->findOneByNameOrSanitizeName($route);
            if (strpos($current, $catalog->getSanitizeName()) !== false) {
                return true;
            }
        }
        return false;
    }

    public function getHeaderImage($current)
    { // (getImageRepo(catalog) ~ catalog.image
        if(preg_match('#^/[^/]*/([^/]*)/?.*$#', $current, $matches)){
            $catalogName = $matches[1];
            /** @var CatalogRepository $catalogRepository */
            $catalogRepository = $this->em->getRepository('CatBundle:Catalog');
            /** @var Catalog $catalog */
            $catalog = $catalogRepository->findOneByNameOrSanitizeName($catalogName);
            if ($catalog instanceof Catalog) {
                return $this->getImageLink($catalog, 'catalogfull');
            }
        }
        return $this->getImageLink('bundles/cat/images/jouets.jpg');
    }

    /**
     * @param mixed  $productOrPath
     * @param string $filter
     * @return string
     */
    public function getImageLink($productOrPath, $filter = 'cache')
    {
        $path = ($productOrPath instanceof AbstractProduct || $productOrPath instanceof Catalog) ?
            $this->getImageRepository($productOrPath) . $productOrPath->getImage() :
            $productOrPath;

        /** @var ImagineHelper $imagineHelper */
        $imagineHelper = $this->container->get('liip_imagine.templating.helper');

        $imagePath = $imagineHelper->filter($path, $filter);
        $this->createImageCache($path, $filter);
        return $imagePath;
    }

    /**
     * Crée le cache de l'image s'il n'existe pas
     * @param string $path
     * @param string $filter
     */
    public function createImageCache($path, $filter)
    {
        if(!file_exists($path)){
            $projectPath = ($this->container->getParameter('host') == 'catalog.localhost')
                ? '/var/www/develop/catalog'
                : '/var/www/catalog';
            $process = new Process('cd ' . $projectPath);
            $process->run();
            $process->setCommandLine('php app/console liip:imagine:cache:resolve ' . $path . ' --filters=' . $filter);
        }
    }
}