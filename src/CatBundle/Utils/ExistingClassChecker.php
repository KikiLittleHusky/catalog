<?php

namespace CatBundle\Utils;

class ExistingClassChecker
{
    /**
     * @param $className
     * @return bool
     */
    public function isCatalogClassExisting($className)
    {
        return (class_exists('CatBundle\Entity\\' . $className));
    }
}