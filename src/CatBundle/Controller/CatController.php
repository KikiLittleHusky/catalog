<?php
namespace CatBundle\Controller;

use CatBundle\Entity\Catalog;
use CatBundle\Entity\Tag;
use CatBundle\Helper\StatsHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CatBundle\Repository\ProductRepository;
use CatBundle\Repository\CatalogRepository;
use CatBundle\Repository\TagRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CatController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        /** @var CatalogRepository $repository */
        $repository = $this->getDoctrine()->getRepository('CatBundle:Catalog');
        $catalogs = $repository->findBy(
            array('activated' => 1)
        );

        /** @var StatsHelper $statsHelper */
        $statsHelper = $this->get('cat.helper_stats');

        $statistics      = $statsHelper->getStatistics($catalogs);
        $latestPurchases = $statsHelper->getLatestPurchases($catalogs);

        return $this->render('CatBundle:Page:home.html.twig', array(
            'catalogs'        => $catalogs,
            'stats'           => $statistics,
            'latestPurchases' => $latestPurchases
        ));
    }

    public function searchAction($query)
    {
        /** @var CatalogRepository $repository */
        $repository = $this->getDoctrine()->getRepository('CatBundle:Catalog');
        $catalogs = $repository->findBy(
            array('activated' => 1)
        );
        $products = array();
        /** @var Catalog $catalog */
        foreach ($catalogs as $catalog) {
            /** @var ProductRepository $repository */
            $repository = $this->getDoctrine()->getRepository('CatBundle:' . $catalog->getClassName());
            $products = array_merge($products, $repository->findAllWithImage(2));
        }

        return $this->render('CatBundle:Page:search.html.twig', array(
            'products' => $products
        ));
    }


    /**
     * @Route("/kiki", name="kiki")
     */
    public function kikiAction()
    {
        return $this->collectionAction('kiki');
    }

    /**
     * @Route("/nanoblock", name="nanoblock")
     */
    public function nanoblockAction()
    {
        return $this->collectionAction('nanoblock');
    }

    /**
     * @Route("/funkopop", name="funkopop")
     */
    public function funkopopAction()
    {
        return $this->collectionAction('funkopop');
    }

    /**
     * @Route("/minis", name="minis")
     */
    public function minisAction()
    {
        return $this->collectionAction('minis');
    }

    /**
     * @Route("/medal", name="medal")
     */
    public function medalAction()
    {
        return $this->collectionAction('medal');
    }

    /**
     * Route dans routing.yml
     * @param $product
     * @return Response
     */
    public function collectionAction($product)
    {
        /** @var CatalogRepository $repositoryCatalog */
        $repositoryCatalog = $this->getDoctrine()->getRepository('CatBundle:Catalog');
        $catalog = $repositoryCatalog->findOneByNameOrSanitizeName($product);

        if (!($catalog instanceof Catalog)
            || !$catalog->getActivated()
            || !$catalog->getHasTagList()
            || !$this->get('cat.classchecker')->isCatalogClassExisting($catalog->getClassName())
        ) {
            return $this->render('CatBundle:Exception:error404.html.twig');
        }

        /** @var TagRepository $repositoryTag */
        $repositoryTag = $this->getDoctrine()->getRepository('CatBundle:Tag');
        $byTag = $repositoryTag->findProductsByTag($catalog->getClassName());

        /** @var ProductRepository $repository */
        $repository = $this->getDoctrine()->getRepository('CatBundle:' . $catalog->getClassName());
        $numberProduct = $repository->countProductsByTagOrNot($catalog->getClassName());

        $template = 'CatBundle:Page:listtag-default.html.twig';
        if ($this->get('templating')->exists(
            'CatBundle:Page:listtag-' . $catalog->getSanitizeName() . '.html.twig'
        )) {
            $template = 'CatBundle:Page:listtag-' . $catalog->getSanitizeName() . '.html.twig';
        }
        return $this->render($template, array(
            'byTag' => $byTag,
            'total' => $numberProduct
        ));
    }

    /**
     * Route dans routing.yml
     * @param $product
     * @return Response
     */
    public function wishlistAction($product)
    {
        /** @var CatalogRepository $repositoryCatalog */
        $repositoryCatalog = $this->getDoctrine()->getRepository('CatBundle:Catalog');
        $catalog = $repositoryCatalog->findOneByNameOrSanitizeName($product);

        if (!($catalog instanceof Catalog)
            || !$catalog->getHasWishlist()
            || !$this->get('cat.classchecker')->isCatalogClassExisting($catalog->getClassName())
        ) {
            return $this->render('CatBundle:Exception:error404.html.twig');
        }
        /** @var ProductRepository $repository */
        $repository = $this->getDoctrine()->getRepository('CatBundle:' . $catalog->getClassName());
        $products = $repository->findAllWithImage(1);

        $template = 'CatBundle:Page:wishlist-default.html.twig';
        if ($this->get('templating')->exists(
            'CatBundle:Page:wishlist-' . $catalog->getSanitizeName() . '.html.twig'
        )) {
            $template = 'CatBundle:Page:wishlist-' . $catalog->getSanitizeName() . '.html.twig';
        }
        return $this->render($template, array(
            'listProducts' => $products
        ));
    }

    /**
     * @param $product
     * @param $nameTag
     * @return Response
     */
    public function tagAction($product, $nameTag)
    {
        /** @var CatalogRepository $repositoryCatalog */
        $repositoryCatalog = $this->getDoctrine()->getRepository('CatBundle:Catalog');
        $catalog = $repositoryCatalog->findOneByNameOrSanitizeName($product);

        if (!($catalog instanceof Catalog)
            || !$catalog->getActivated()
            || (!$catalog->getHasTagList() && $nameTag !== 'all')
            || !$this->get('cat.classchecker')->isCatalogClassExisting($catalog->getClassName())
        ) {
            return $this->render('CatBundle:Exception:error404.html.twig');
        }

        /** @var TagRepository $repository */
        $tagRepository = $this->getDoctrine()->getRepository('CatBundle:Tag');
        $tag = $tagRepository->findOneBy(array('sanitizeName' => $nameTag));

        if ($tag !== null || $nameTag == 'all') {
            /** @var ProductRepository $repository */
            $repository = $this->getDoctrine()->getRepository('CatBundle:' . $catalog->getClassName());
            if ($nameTag == 'all') {
                if (!$catalog->getHasAllList()) {
                    return $this->render('CatBundle:Exception:error404.html.twig');
                }
                $products = $repository->findAllWithImageByTag(0);
                $nameTag = "Tous";
            } else {
                $products = $repository->findAllWithImageByTag(0, $tag->getId());
                $nameTag = $tag->getLabel();
            }

            $template = 'CatBundle:Page:intag-default.html.twig';
            if ($this->get('templating')->exists(
                'CatBundle:Page:intag-' . $catalog->getSanitizeName() . '.html.twig'
            )) {
                $template = 'CatBundle:Page:intag-' . $catalog->getSanitizeName() . '.html.twig';
            }
            return $this->render($template, array(
                'listProducts'  => $products,
                'nameProduct'   => $catalog->getName(),
                'nameTag'       => $nameTag
            ));
        }
        return $this->render('CatBundle:Exception:error404.html.twig');
    }
}
